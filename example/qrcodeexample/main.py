from kivy.lang import Builder
from kivy.app import App
from zbar_qrcode import ZbarQrcodeDetector

if __name__ == '__main__':

    qrcode_kv = '''
BoxLayout:
    orientation: 'vertical'

    ZbarQrcodeDetector:
        id: detector

    Label:
        text: '\\n'.join(map(repr, detector.symbols))
        size_hint_y: None
        height: '100dp'

    BoxLayout:
        size_hint_y: None
        height: '48dp'

        Button:
            text: 'Scan a qrcode'
            on_release: detector.start()
        Button:
            text: 'Stop detection'
            on_release: detector.stop()
'''

    class QrcodeExample(App):
        def build(self):
            return Builder.load_string(qrcode_kv)

    QrcodeExample().run()

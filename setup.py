#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from setuptools import setup
import os
import io
import re


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


def get_version():
    init = read(os.path.join('zbar_qrcode', '__init__.py'))
    return re.search('__version__ = "([0-9.]*)"', init).group(1)


version = get_version()

setup(name='zbar_qrcode',
    version=version,
    description='ZBar QRcode scanner for kivy android app',
    long_description=read('README.md'),
    author='Datalife S.Coop.',
    author_email='info@datalifeit.es',
    url='https://gitlab.com/datalifeit/kivy-zbar_qrcode',
    license='GPL-3',
    packages=['zbar_qrcode'],
    package_data={
      'zbar_qrcode': ['*.py']
    },
    install_requires=['kivy', 'python-for-android'])
